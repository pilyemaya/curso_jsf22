package es.luceit.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.chart.MeterGaugeChartModel;

@ManagedBean
@ViewScoped
public class GaugeBean implements Serializable {

	private static final long serialVersionUID = 569346129227073933L;

	private static final int ROJO = 100;
	private static final int AMARILLO = 80;
	private static final int VERDE = 50;

	private Long valorActual = 0L;
	private MeterGaugeChartModel gaugeModel;
	private ArrayList<Number> intervals;

	public void mostrarGauge() {
		gaugeModel = new MeterGaugeChartModel(valorActual, intervals);
	}

	@PostConstruct
	public void init() {
		intervals = new ArrayList<Number>();
		intervals.add(VERDE);
		intervals.add(AMARILLO);
		intervals.add(ROJO);

	}

	public MeterGaugeChartModel getGaugeModel() {
		return gaugeModel;
	}

	public Long getValorActual() {
		return valorActual;
	}

	public void setValorActual(final Long valorActual) {
		this.valorActual = valorActual;
	}

}
