package es.luceit.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class GalleriaBean implements Serializable {

	private static final long serialVersionUID = 4109675670923140827L;

	private List<String> images = new ArrayList<String>();

	@PostConstruct
	public void init() {

		for (int i = 1; i <= 6; i++) {
			images.add("flowers" + i + ".jpg");
		}

	}

	public List<String> getImages() {
		return images;
	}

	public void setImages(final List<String> images) {
		this.images = images;
	}

}
