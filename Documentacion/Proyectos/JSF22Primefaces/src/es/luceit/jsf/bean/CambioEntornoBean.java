package es.luceit.jsf.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class CambioEntornoBean implements Serializable {

	private static final long serialVersionUID = -7349052319170289568L;

	private Map<String, String> temas = new HashMap<String, String>();

	@PostConstruct
	public void init() {
		temas.put("Afterdark", "afterdark");
		temas.put("Aristo", "aristo");
		temas.put("Black-Tie", "black-tie");
		temas.put("Cruze", "cruze");
		temas.put("Dot-Luv", "dot-luv");
		temas.put("Le-frog", "le-frog");
		temas.put("Mint-choc", "mint-choc");

	}

	public Map<String, String> getTemas() {
		return temas;
	}

	public void setTemas(final Map<String, String> temas) {
		this.temas = temas;
	}
}
