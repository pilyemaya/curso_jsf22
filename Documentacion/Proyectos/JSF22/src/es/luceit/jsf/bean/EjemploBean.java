package es.luceit.jsf.bean;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class EjemploBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private static final String NOMBRE_CORRECTO = "Juan";
	private static final String PASSWORD_CORRECTO = "1234";
	private String nombre = "Pilar";
	private String password;
	
	public String login() {
		if(!nombre.equals(NOMBRE_CORRECTO)||!password.equals(PASSWORD_CORRECTO)){
			return "error?faces-redirect=true";
		}
		return "home?faces-redirect=true";
	}


	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}
}
