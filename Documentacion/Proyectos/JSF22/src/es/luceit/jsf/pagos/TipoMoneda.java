package es.luceit.jsf.pagos;

public class TipoMoneda {
	
	private String nombre;
	private String abreviatura;
	
	public TipoMoneda(){
		
	}
	
	public TipoMoneda(final String nombre, final String abreviatura){
		this.nombre=nombre;
		this.abreviatura=abreviatura;
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getAbreviatura() {
		return abreviatura;
	}

	public void setAbreviatura(String abreviatura) {
		this.abreviatura = abreviatura;
	}


}
