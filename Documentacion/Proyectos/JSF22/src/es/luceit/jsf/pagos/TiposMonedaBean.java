package es.luceit.jsf.pagos;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean
@ApplicationScoped
public class TiposMonedaBean {

	public List<TipoMoneda> getTiposMoneda() {
		final List<TipoMoneda> tiposMoneda = new ArrayList<TipoMoneda>();
		tiposMoneda.add(new TipoMoneda("Euro", "€"));
		tiposMoneda.add(new TipoMoneda("Dolar", "$"));
		return tiposMoneda;
	}
}
