package es.luceit.jsf.pagos;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;

@ManagedBean
public class PagoBean implements Serializable {

	private static final long serialVersionUID = -6945411589474067894L;

	private Pago pago = new Pago();

	@ManagedProperty(value = "#{listadoPagosBean}")
	private ListadoPagosBean listadoPagosBean;

	public String anhadirPago() {
		if (pago.getTipo().equals("gasto") && pago.getCantidad() > 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"Si es un gasto la cantidad debe ser negativa"));
			return null;
		}

		if (pago.getTipo().equals("ingreso") && pago.getCantidad() < 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"Si es un ingreso la cantidad debe ser positiva"));
			return null;
		}
		listadoPagosBean.getPagos().add(pago);
		return "listadoPagos?faces-redirect=true";
	}

	public void setListadoPagosBean(ListadoPagosBean listadoPagosBean) {
		this.listadoPagosBean = listadoPagosBean;
	}

	public Pago getPago() {
		return pago;
	}

	public void setPago(Pago pago) {
		this.pago = pago;
	}

}
