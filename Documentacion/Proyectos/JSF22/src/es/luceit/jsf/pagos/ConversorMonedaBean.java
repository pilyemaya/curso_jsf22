package es.luceit.jsf.pagos;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

@ManagedBean
@ViewScoped
public class ConversorMonedaBean implements Serializable {

	private static final long serialVersionUID = 6337710271832795069L;
	private Double cantidad;
	private String tipoMoneda;
	private Double resultado;

	public void convertirMoneda(ActionEvent event) {
		if ("Euro".equals(tipoMoneda)) {
			resultado = cantidad;
		} else if ("Dolar".equals(tipoMoneda)) {
			resultado = cantidad * 1.12;
		}
	}

	public Double getCantidad() {
		return cantidad;
	}

	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}

	public String getTipoMoneda() {
		return tipoMoneda;
	}

	public void setTipoMoneda(String tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}

	public Double getResultado() {
		return resultado;
	}

	public void setResultado(Double resultado) {
		this.resultado = resultado;
	}
}
