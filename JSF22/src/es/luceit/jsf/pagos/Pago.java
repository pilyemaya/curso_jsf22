package es.luceit.jsf.pagos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Pago implements Serializable {

	private static final long serialVersionUID = -6945411589474067894L;

	private String tipo;
	private Double cantidad;
	private String descripcion;
	private Date fecha;
	private boolean imprevisto;
	private String tipoMoneda;
	private List<String> categorias = new ArrayList<String>();

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Double getCantidad() {
		return cantidad;
	}

	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public boolean isImprevisto() {
		return imprevisto;
	}

	public void setImprevisto(boolean imprevisto) {
		this.imprevisto = imprevisto;
	}

	public String getTipoMoneda() {
		return tipoMoneda;
	}

	public void setTipoMoneda(String tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}

	public List<String> getCategorias() {
		return categorias;
	}

	public void setCategorias(List<String> categorias) {
		this.categorias = categorias;
	}

}
