package es.luceit.jsf.pagos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class ListadoPagosBean implements Serializable {

	private static final long serialVersionUID = 923284923980857697L;
	private List<Pago> pagos = new ArrayList<Pago>();

	public List<Pago> getPagos() {
		return pagos;
	}

	public void setPagos(List<Pago> pagos) {
		this.pagos = pagos;
	}

}
