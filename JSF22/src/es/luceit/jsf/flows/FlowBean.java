package es.luceit.jsf.flows;

import javax.faces.bean.ManagedBean;
import javax.faces.flow.FlowScoped;

/* FlowScoped: quiere decir que este bean va a estar vivo durante 
 * el tiempo de vida del flujo flowA.*/

@ManagedBean
@FlowScoped(value = "flowA")
public class FlowBean {

	public String getName() {
		return "FlowBasico";
	}

	public String getReturnValue() {
		return "/return1";
	}

}
