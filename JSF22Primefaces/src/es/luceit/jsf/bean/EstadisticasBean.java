package es.luceit.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;

@ManagedBean
@ViewScoped
public class EstadisticasBean implements Serializable {

	private static final String ESTADISTICA_TIPO_LINE = "Line";
	private static final String ESTADISTICA_TIPO_BAR = "Bar";
	private static final String ESTADISTICA_TIPO_AREA = "Area";
	private static final String IRLANDA = "Irlanda";
	private static final String ALEMANIA = "Alemania";
	private static final String ESPANA = "España";

	private static final long serialVersionUID = -8675113988442993066L;

	private String selectedTypeStatistical;
	private List<String> statisticals = new ArrayList<String>();
	private List<String> typeStatisticals = new ArrayList<String>();
	private final List<ChartSeries> series = new ArrayList<ChartSeries>();
	private CartesianChartModel categoryModel;

	public void verEstadistica() {
		categoryModel = new CartesianChartModel();

		for (final ChartSeries chartSeries : series) {
			categoryModel.addSeries(chartSeries);
		}
	}

	public Boolean isArea() {
		return ESTADISTICA_TIPO_AREA.equals(selectedTypeStatistical);
	}

	public Boolean isBar() {
		return ESTADISTICA_TIPO_BAR.equals(selectedTypeStatistical);
	}

	public Boolean isLine() {
		return ESTADISTICA_TIPO_LINE.equals(selectedTypeStatistical);
	}

	@PostConstruct
	public void init() {
		statisticals.add(ESPANA);
		statisticals.add(ALEMANIA);
		statisticals.add(IRLANDA);

		typeStatisticals.add(ESTADISTICA_TIPO_AREA);
		typeStatisticals.add(ESTADISTICA_TIPO_BAR);
		typeStatisticals.add(ESTADISTICA_TIPO_LINE);

		final ChartSeries tasaParoEspana = new ChartSeries();
		tasaParoEspana.setLabel(ESPANA);
		tasaParoEspana.set("1980", 12);
		tasaParoEspana.set("1982", 14);
		tasaParoEspana.set("1984", 16);
		tasaParoEspana.set("1986", 17);
		tasaParoEspana.set("1988", 16);
		tasaParoEspana.set("1990", 13);
		tasaParoEspana.set("1992", 14);
		tasaParoEspana.set("1994", 19);
		tasaParoEspana.set("1996", 18);
		tasaParoEspana.set("1998", 15);
		tasaParoEspana.set("2000", 12);
		tasaParoEspana.set("2002", 11);
		tasaParoEspana.set("2004", 11);
		tasaParoEspana.set("2006", 8);
		tasaParoEspana.set("2008", 12);
		tasaParoEspana.set("2010", 19);
		tasaParoEspana.set("2012", 22);
		tasaParoEspana.set("2014", 6);

		final ChartSeries tasaParoAlemania = new ChartSeries();
		tasaParoAlemania.setLabel(ALEMANIA);
		tasaParoAlemania.set("1980", 4);
		tasaParoAlemania.set("1982", 7);
		tasaParoAlemania.set("1984", 7);
		tasaParoAlemania.set("1986", 6);
		tasaParoAlemania.set("1988", 6);
		tasaParoAlemania.set("1990", 6);
		tasaParoAlemania.set("1992", 5);
		tasaParoAlemania.set("1994", 7);
		tasaParoAlemania.set("1996", 8);
		tasaParoAlemania.set("1998", 9);
		tasaParoAlemania.set("2000", 7);
		tasaParoAlemania.set("2002", 7);
		tasaParoAlemania.set("2004", 9);
		tasaParoAlemania.set("2006", 12);
		tasaParoAlemania.set("2008", 8);
		tasaParoAlemania.set("2010", 7);
		tasaParoAlemania.set("2012", 6);
		tasaParoAlemania.set("2014", 25);

		final ChartSeries tasaParoIrlanda = new ChartSeries();
		tasaParoIrlanda.setLabel(IRLANDA);
		tasaParoIrlanda.set("1980", 12);
		tasaParoIrlanda.set("1982", 14);
		tasaParoIrlanda.set("1984", 16);
		tasaParoIrlanda.set("1986", 16);
		tasaParoIrlanda.set("1988", 16);
		tasaParoIrlanda.set("1990", 13);
		tasaParoIrlanda.set("1992", 15);
		tasaParoIrlanda.set("1994", 14);
		tasaParoIrlanda.set("1996", 12);
		tasaParoIrlanda.set("1998", 8);
		tasaParoIrlanda.set("2000", 4);
		tasaParoIrlanda.set("2002", 4);
		tasaParoIrlanda.set("2004", 5);
		tasaParoIrlanda.set("2006", 4);
		tasaParoIrlanda.set("2008", 5);
		tasaParoIrlanda.set("2010", 13);
		tasaParoIrlanda.set("2012", 16);
		tasaParoIrlanda.set("2014", 33);

		series.add(tasaParoEspana);
		series.add(tasaParoAlemania);
		series.add(tasaParoIrlanda);
	}

	public List<String> getStatisticals() {
		return statisticals;
	}

	public void setStatisticals(final List<String> statisticals) {
		this.statisticals = statisticals;
	}

	public List<String> getTypeStatisticals() {
		return typeStatisticals;
	}

	public void setTypeStatisticals(final List<String> typeStatisticals) {
		this.typeStatisticals = typeStatisticals;
	}

	public String getSelectedTypeStatistical() {
		return selectedTypeStatistical;
	}

	public void setSelectedTypeStatistical(final String selectedTypeStatistical) {
		this.selectedTypeStatistical = selectedTypeStatistical;
	}

	public CartesianChartModel getCategoryModel() {
		return categoryModel;
	}

	public void setCategoryModel(final CartesianChartModel categoryModel) {
		this.categoryModel = categoryModel;
	}

}
