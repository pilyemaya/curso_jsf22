package es.luceit.jsf.bean;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@ViewScoped
public class LoginBean implements Serializable {

	private static final long serialVersionUID = 5450317013213236311L;

	private static final String CORRECTOS = "Correctos";
	private static final String INCORRECTOS = "Incorrectos";
	private static final String DATOS_DE_USUARIO = "Datos de usuario";
	private static final String LOGIN_INICIAL = "Cesar";
	private static final String PASSWORD_INICIAL = "1234";
	private static final String PASSWORD_CAMBIADO_CORRECTAMENTE = "Password cambiado correctamente";
	private static final String MESSAGES_LOGIN_ID = "messagesLogin";

	private String login = LOGIN_INICIAL;
	private String password = PASSWORD_INICIAL;
	private String passwordCambiar;
	private String passwordPermanente = PASSWORD_INICIAL;

	public void intertarLogearse() {
		FacesMessage facesMessage = null;
		final FacesContext context = FacesContext.getCurrentInstance();
		if (LOGIN_INICIAL.equals(login) && passwordPermanente.equals(password)) {
			facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO,
					DATOS_DE_USUARIO, CORRECTOS);
		} else {
			facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR,
					DATOS_DE_USUARIO, INCORRECTOS);
		}
		context.addMessage(MESSAGES_LOGIN_ID, facesMessage);

	}

	public void cambiarPassword() {
		this.passwordPermanente = this.passwordCambiar;
		final FacesContext context = FacesContext.getCurrentInstance();
		final FacesMessage facesMessage = new FacesMessage(
				FacesMessage.SEVERITY_INFO, PASSWORD_CAMBIADO_CORRECTAMENTE, "");
		context.addMessage(MESSAGES_LOGIN_ID, facesMessage);
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(final String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(final String password) {
		this.password = password;
	}

	public String getPasswordCambiar() {
		return passwordCambiar;
	}

	public void setPasswordCambiar(final String passwordCambiar) {
		this.passwordCambiar = passwordCambiar;
	}

}
