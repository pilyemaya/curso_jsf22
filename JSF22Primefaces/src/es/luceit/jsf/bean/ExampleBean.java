package es.luceit.jsf.bean;

import java.io.Serializable;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@SessionScoped
public class ExampleBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String name;
	private Date currentDate = new Date();

	public void mostrarDatosDeEntrada() {
		final FacesContext context = FacesContext.getCurrentInstance();
		final FacesMessage facesMessage = new FacesMessage(
				FacesMessage.SEVERITY_INFO, "Datos introducidos", "Name: "
						+ name + " ---- Fecha: " + currentDate.toString());
		context.addMessage("messages", facesMessage);
	}

	public Date getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(final Date currentDate) {
		this.currentDate = currentDate;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

}
